#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
	lastPoint = QPoint(0, 0);
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	painted = true;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	painted = true;
	update();
	lastPoint = QPoint(0, 0);
	transformacia = false;
	V.clear();
	return true;

}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}


void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	QPoint bod;
	//nacitava body n-uholnika a vykresluje usecky
	if (painted&&event->button() == Qt::LeftButton&&!transformacia) {
		if (V.size() > 0) 
			dda(V[V.size() - 1].x(), V[V.size() - 1].y(), event->pos().x(), event->pos().y());
		V.push_back(event->pos());
		update();
//		painting = true;
	}
	//nastavi centralny bod transformacie
	if (transformacia&&event->button() == Qt::LeftButton)
		lastPoint = event->pos();
	//vykresli poslednu usecku a spusti scanline
	//prepne tiez program na pracu s transformaciami
	if (V.size() && event->button() == Qt::RightButton) {
		dda(V[V.size() - 1].x(), V[V.size() - 1].y(), V[0].x(), V[0].y());
		rob();
		transformacia = true;
	}
		
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
//	if ((event->buttons() & Qt::LeftButton) && painting)
//		drawLineTo(event->pos());
}

//pustenie my�ky uskuto�n� posunutie
void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && transformacia)
		posun(event->pos());
	
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
/*  QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;*/
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

//scanline algoritmus - tvorba hran a tabulky aktualnych hran
void PaintWidget::rob() {
	bool x=false;
	int n = V.size() - 1;
	std::vector<priamka> tah;
	priamka* P = new priamka[n+1];
	int min=600, max=0;
	update();

	for (int i = 0;i < n;i++)
		if (V[i].y() < V[i + 1].y()) {
			P[i].nastav(V[i], V[i + 1]);
			if (V[i].y() < min) min = V[i].y();
			if (V[i + 1].y() > max) max = V[i + 1].y();
		}
		else {
			P[i].nastav(V[i + 1], V[i]);
			if (V[i].y() > max) max = V[i].y();
			if (V[i + 1].y() < min) min = V[i + 1].y();
		}
		if (V[0].y() < V[n].y()) P[n].nastav(V[0], V[n]);
		else P[n].nastav(V[n], V[0]);

	for (int y = min;y <= max;y++) {
		for (int i = 0;i <= n;i++) {
			if (!(P[i] != y)) {
				tah.push_back(P[i]);
				P[i].iter();
			}
		}
		if (!tah.empty())
			vypln(y, tah);
		tah.clear();
	}
//	V.clear();
	delete[] P;
	update();
}

//funkcia scanline pracujuca uz len s tab. aktu. hran
void PaintWidget::vypln(int y, std::vector<priamka> P) {

	std::sort(P.begin(), P.end());

	for (int i = 0;i < P.size();i+=2) {
		for (int x = P[i].X();x < P[i + 1].X();x++) {
			if (!mimo(x,y))image.setPixelColor(x, y, myPenColor);
		}
		P[i].iter();
		P[i + 1].iter();
	}
}

//dda algoritmus
void PaintWidget::dda(int zx, int zy, int kx, int ky) {

	if (abs(ky - zy) > abs(kx - zx)) {
		double k = (kx - zx) / (double)(ky - zy);
		double q = kx - k*ky;
		if (zy > ky) for (int y = zy; y > ky;y--) image.setPixelColor(k*y + q, y, QColor(0,0,0));
		else for (int y = zy;y < ky;y++) image.setPixelColor(k*y + q, y, QColor(0, 0, 0));
	}
	else {
		double k = (ky - zy) / (double)(kx - zx);
		double q = ky - k*kx;
		if (zx > kx)for (int x = zx;x > kx;x--) image.setPixelColor(x, k*x + q, QColor(0, 0, 0));
		else for (int x = zx;x < kx;x++) image.setPixelColor(x, k*x + q, QColor(0, 0, 0));
	}
}

//posunutie
void PaintWidget::posun(QPoint B) {
	B -= lastPoint;

	for (int i = 0; i < V.size(); i++) V[i] += B;

	clearImage();

	rob();
	obrysy();
}

//otocenie o uhol 
void PaintWidget::otocenie(double uhol) {
	double SIN = sin(uhol);
	double COS = cos(uhol);
	int x;

	for (int i = 0; i < V.size(); i++) {
		V[i] -= lastPoint;
		x = V[i].x();
		V[i].setX((int)(COS*x + SIN*(V[i].y())));
		V[i].setY((int)(-SIN*x + COS*(V[i].y())));
		V[i] += lastPoint;
	}
	clearImage();
	rob();
	obrysy();
}

//skalovanie v smere x aj y
void PaintWidget::skalovanie(double kx,double ky) {

	for (int i = 0; i < V.size(); i++) {
		V[i] -= lastPoint;
		V[i].setX(V[i].x()*kx);
		V[i].setY(V[i].y()*ky);
		V[i] += lastPoint;
	}
	clearImage();
	rob();
	obrysy();
}

//skosenie v smere x
void PaintWidget::skosenieX(double b) {

	for (int i = 0; i < V.size(); i++) {
		V[i] -= lastPoint;
		V[i].setX(V[i].x() + b*V[i].y());
		V[i] += lastPoint;
	}
	clearImage();
	rob();
	obrysy();	
}

//analogia predoslej funkcie
void PaintWidget::skosenieY(double c) {

	for (int i = 0;i < V.size();i++) {
		V[i] -= lastPoint;
		V[i].setY(c*V[i].x() + V[i].y());
		V[i] += lastPoint;
	}
	
	clearImage();
	rob();
	obrysy();
}

//funkcia preklopi obrazok podla priamky rovnobeznej s y 
//prechadzajucej zadanym bodom
void PaintWidget::preklopy() {
	clearImage();
	int x = lastPoint.x();
	QPoint X(x, 0);
	for (int i = 0;i < V.size();i++) {
		V[i] -= X;
		V[i].setX(-1 * V[i].x());
		V[i] += X;
	}
	rob();
	obrysy();
	for (int y = 0;y < 800;y++)
		image.setPixelColor(x, y, QColor(255, 0, 0));

}

//analogia predoslej funkcie
void PaintWidget::preklopx() {
	int y = lastPoint.y();
	clearImage();
	QPoint Y(0, y);
	for (int i = 0;i < V.size();i++) {
		V[i] -= Y;
		V[i].setY(-1*V[i].y());
		V[i] += Y;
	}
	rob();
	obrysy();
	for (int x = 0;x < 800;x++)
		image.setPixelColor(x, y, QColor(255, 0, 0));

}

//funkcia vykresli usecky tvoriace n-uholnik
void PaintWidget::obrysy() {
	for (int i = 1;i < V.size();i++)
		dda(V[i].x(), V[i].y(), V[i - 1].x(), V[i - 1].y());
	dda(V[0].x(), V[0].y(), V[V.size()-1].x(), V[V.size()-1].y());
}

//funkcia testuje, ci je pixel mimo obrazka
bool PaintWidget::mimo(int x, int y){
	if (x < 0) return true;
	if (y < 0) return true;
	if (x >= image.width()) return true;
	if (x >= image.height()) return true;
	return false;

}