#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 800);
}

void MyPainter::spusti() {
	if (ui.radioButton_4->isChecked()) paintWidget.otocenie(M_PI*ui.spinBox->value()/180);
	if (ui.radioButton_3->isChecked()) paintWidget.skalovanie(ui.doubleSpinBox->value(),ui.doubleSpinBox_4->value());
	if (ui.radioButton_2->isChecked()) paintWidget.skosenieX(ui.doubleSpinBox_2->value());
	if (ui.radioButton->isChecked()) paintWidget.skosenieY(ui.doubleSpinBox_3->value());
	if (ui.radioButton_5->isChecked()) paintWidget.preklopx();
	if (ui.radioButton_6->isChecked()) paintWidget.preklopy();
}

void MyPainter::plocha() {
	paintWidget.setPenColor(QColor(ui.spinBox_7->value(), ui.spinBox_2->value(), ui.spinBox_6->value()));
}